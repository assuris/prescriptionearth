+++
date = 2021-04-05T04:00:00Z
featureImage = "/images/pxl_20210307_185832954-portrait-2.jpg"
postImage = "/images/pxl_20210307_200328816-2.jpg"
title = "What To Expect From Your First Acupuncture Treatment"

+++
This week's blog is a step-by-step walkthrough of our first appointment together:

Before your appointment I will email you paperwork to fill out.  When you arrive at the office, text me and I will take your temperature at your car.

Your first session with me will be an hour and a half.  Approximately the first 30 minutes will be talking about why you came in and an extensive intake of your health history.

Next, I will spend about 5-10 minutes feeling your pulses (right and left wrist), and looking at your tongue.  This is how I was taught to form a Traditional Chinese Medicine diagnosis.  The tongue and pulse tell me many things about what is going on inside the body, and can guide me to appropriate acupuncture points and herbs for your formula.  During this part of the session, all you have to do is lay on the table, and relax.

By the end of tongue and pulse, I will have chosen acupuncture points.  Then we will discuss whether you will lay on your front, back, side, or occasionally in special circumstances you may sit in a comfortable chair.  We will also decide how much clothing to disrobe based on where needles will be inserted and your comfort level.  I will always drape appropriately.

After you are on the table and comfortable, I will walk you gently through inserting a needle.  I will adjust my needling style based on your comfort.  (Some people want me to move quickly, while others like to breathe in and out with every needle).

I usually insert 10-20 needles very superficially.  All of my needles are single-use, filiform (not hollow), sterile, and about as fine as a human hair.  Most people describe the feeling as a mosquito bite, which disappears quickly.  Sometimes you won't even feel the needle, sometimes you may feel a slight ache around the insertion, or a slight burning or tingling.  All which go away quickly, and I encourage you to tell me what you are feeling.

Don't be surprised if needles do not go exactly where you think they would go, or where your pain or ailment is.  This is all based on the flow of qi through pathways, or meridians in the body.  Once the needles are in and you are completely comfortable, I will exit the room and let you relax for 20-40 minutes depending on the treatment.  Acupuncture helps you to relax, so as much as you are able, let go, relax, or take a nap.

Before, during or after the needle insertion, I may also use additional techniques, such as cupping, massage or moxibustion (warming the skin with mugwort).  I will walk you through all of these techniques if they are part of your treatment.

Toward the end of your treatment, I will come back in to gather the herbs for your first formula (included in the price of your first treatment).  And I will go over these instructions after the treatment.

I always ask you to return the following week if you are able, so I can follow up with you.  90% of my patients feel a shift in their condition after the first session.  Usually there is relief from your condition for 3-5 days.  These are generalizations based on my experience.  Acupuncture is cumulative, so treatments will keep getting better, and herbs enhance the healing process.  At your second session we will come up with a plan on how frequent you will need to return.  This is based on chronicity and severity of symptoms and how well you reacted to your first session.