+++
date = 2021-06-17T14:00:00Z
featureImage = "/images/brain-gut.jpg"
postImage = "/images/guthealthbanner1603795871.jpg"
title = "A Holistic Approach to IBS"

+++
In Chinese Medicine, everything is about patterns and relationships. When a patient comes to me seeking help with Irritable Bowel Syndrome, Irritable Bowel Disease, Ulcerative Colitis, or Chrones Disease, I ask about the patient's emotional life.

If there is a strong correlation between the patient's digestion and emotions, there is a good chance that the relationship between the Liver and the Spleen is out of balance.

The liver becomes stressed by toxins, but also by anger, repressed emotion, resentments and long standing frustration. The liver has a strong tendency to "act out" on the Spleen.

If there is a weakness of Spleen qi, or if the Liver is particularly angry, then the healthy dynamic of the Spleen and the Spleen's relationship with the stomach and intestines is disrupted.

Most of these patients have tried many different elimination diets, but if the Liver and emotions are not addressed in this pattern, the results will be minimal.

It is my strategy, and it is very important to boost Spleen qi, but is is equally important to calm the emotions and soothe the Liver.

This is holistic medicine at it's best. I look at and treat the whole body. If there are harmonious relationships between the organs, the human body can handle quite a bit of physical and emotional stress. When the organs are happy, we are happy, and we have less pain and discomfort, fewer food sensitivities and a relaxed and harmonious relationship with food.

To learn more, book a free consultation with me today!