+++
date = 2022-02-01T05:00:00Z
featureImage = "/images/tiger.jpg"
postImage = "/images/tiger2022v2.jpg"
title = "Happy Lunar New Year, 2022!"

+++
I love Chinese New Year because it gives me an excuse to re-start 😊. I always feel a bit blah right after January 1st. A little guilty for depleting my bank account and eating too much sugar. Also, there's too much pressure to hit the ground running, after all the stress of Christmas! In short- there is an excess of stress and a lack of self care through the holidays! This year, it just did not feel right to forge ahead without proper rest and reflection. And so, I used the time in between January 1st and now to rest, recuperate and plan for the year of the Yang Water Tiger. This year, the celebration is Tuesday, February 1st.

A LOT has happened since I started Prescription Earth in March of 2021! I now realize that I have not allowed myself enough time and space to process it all, on top of personal life changes. January 1st, I was not quite ready for 2022. But now, with a little help from my friends and some acupuncture, I am grounded in my vision of what I want this year to look like.

I don’t have to live my life by anyone else’s timeline or expectations. My resolution is general and simple. I need to do more of what feels really good, and less of what does not.

Today I want to share with you my own personal daily reading. Yes, I read it every day to help me refocus my purpose in my practice and my precious time on this earth. I spent 2021 adding to and refining this list. My hope is that you can make it personal, and find it relevant to your own life. How we begin and end our year is so important, but it stems from how we begin and end each day.

Happy Lunar New Year!  
Melissa

—---

Every day, I am healing myself

and facilitating healing of the entire planet

through a ripple effect.

I believe in miraculous healing

by listening to my heart.

By allowing wildness

I can set myself free

from limiting beliefs & constructs.

I can be

whoever I want to be

at any moment.

I am the author of my own story.

I am a co-creator

of the ever-evolving universe.

I am secure,

powerful,

and benevolent.

My heart is the fearless leader in my life,

and I stand in my truth.

I have clarity and confidence in all of my intuitions

and all of my decisions.

.

I have spaciousness within my body and my life.

I have all of the resources I need,

and personal boundaries

to live my life on my terms.

All of my relationships are authentic and harmonious.

I love myself deeply- body, mind and spirit.

I love deeply and passionately,

and I am loved deeply and passionately.

I am brave, blissful, creative, and playful.

I do not hold on to resistance or guilt.

I have more than enough money,

more than enough time,

plenty of room to grow,

and freedom to create, with ease.

I feel a sense of security around my future.

I will always be able to help my patients, my family, and my friends in need of support.

My creative energy is constantly renewed,

and I am surrounded by people who inspire me.

I have enough "me" time.

I live a harmonious, and sustainable life

and I inspire others to do the same.

My days are filled with peace and joy.

I have financial abundance and peace-of-mind.

My home is beautiful and peaceful.

How I present myself to the world

is in alignment with who I am.

I radiate authenticity, confidence and Love.

I have impeccable boundaries

like river banks.

The rich fertile earth

helps me to flow wild and free

with direction and purpose.

I see good in everyone,

and everything.

and I am an expression of love.

How I live and love matters.

My existence is my purpose.

and I am always in the right place at the right time.