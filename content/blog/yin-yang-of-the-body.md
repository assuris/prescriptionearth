+++
date = 2021-03-14T05:00:00Z
featureImage = "/images/metal-yin-yang-symbol.jpg"
postImage = "/images/images-1.jpg"
title = "Creating Balance: The Yin & Yang of The Body"

+++
Yin and Yang are at the heart of East Asian Medicine. Part of what I do is evaluate the state of Yin and Yang in the body, and help you to move out of extremes and into homeostasis, which is to say, the balance of Yin and Yang in the body.

Let's zoom out for a minute and look at the bigger picture. All of the universe is made up of Yin and Yang. Think of Yin as the night, and Yang as the day. They are opposites, but are always transforming into each other, and need each other to exist. All things have both Yin and Yang qualities, depending on the context and what you’re comparing it to.

To give you a better sense:

Yin is:

Dark

Cool or cold

Moist or wet

Receptive

Attractive (as opposed to pushy)

Slow

Moves inward

Moves downward

Feminine

Form (as opposed to function)

Being (as opposed to doing)

Decay (as opposed to growth)

Earth

Material

Heavy

Sinking

Soft

Curvy

Yang is:

Bright

Warm or hot

Arid and dry

Giving

Pushy

Fast

Moves up and out

Masculine

Function (as opposed to form)

Doing (as opposed to being)

Growth (as opposed to decay)

Heaven

Ethereal

Light

Floating

Sharp

Linear

So as you can see, we all exhibit both Yin and Yang qualities in our own lives, and this can easily get out of balance. We get burned out from using up our Yang energy, and we feel lost and flounder if we have too much Yin. I personally feel out of balance when I am “doing” more than “being” and giving more than receiving.

When you come into Prescription Earth, I will evaluate the state of Yin and Yang in your body, based upon the symptoms that you present with, also what I see on your tongue and feel in your pulse. I will choose acupuncture points and herbs specific for these patterns. I also will go over things in your daily life that may help you outside of the clinic, such as dietary changes, and how to get in sync with daily and seasonal circadian rhythms.