+++
date = 2022-02-24T05:00:00Z
featureImage = "/images/regardt-van-der-meulen-fragmented-metal-sculptures-9-1.jpg"
postImage = "/images/regardt-van-der-meulen-fragmented-metal-sculptures-9.jpg"
title = "A Revolutionary Approach: Treat the Person, Not Just the Symptoms"

+++
Naturally, we all want the smartest, brightest, and ‘best’ doctors working on our team. We want our practitioner to have the most experience, knowledge, expertise, training and education. These are, indeed, great qualities and standards to have! Our minds are incredible tools.

However, healing is a journey, and not solely an intellectual problem to solve. Healing takes rebalancing, unwinding, and the spaciousness and time to do such things! It takes a considerable amount of evaluation of your physical and emotional needs that may or may not be fulfilled. Sometimes it takes a complete overhaul of your current lifestyle, with an accountability partner, and/or a gentle helping hand. But most importantly, true healing takes getting in touch with our bodies. In my opinion, the patient MUST become in tune with their body for true healing to take place. This takes BODY work.

When a patient has cancer, I do not consider the tumor an enemy that invokes hatred and punishment. I see the cancer as a part of the whole picture. Perhaps the parts of the patient that have become lost, fragmented, forgotten, or undifferentiated. I see the tumor as a teacher that is showing us an imbalance that needs attention. The parts of us all that need long to be rectified. A cancer tumor is actually trying to _help_ your body by containing the cancerous cells. Let’s decode the messages that the body is sending, together.

Based on this discussion, if you are in search of healing, here are a few questions you may want to add to your list of prerequisites when choosing a health care provider. (My training emphasized what we call 'holding space' for patients.)

\-Does my practitioner care about me?

\-Does my practitioner listen to me?

\-Do I connect with my practitioner?

\-Does my practitioner believe me and empathize with me?

\-Does my practitioner give me the time and space to express my concerns and needs?

\-Does my practitioner have tools besides drugs or surgery?

\-Does my practitioner have any lifestyle suggestions?

\-Does my practitioner speak in a way that I can understand and relate to?

\-Does my practitioner try to answer all of my questions?

\-Does my practitioner incorporate bodywork, and help me connect with my body?

Sometimes problems are too big, too overwhelming, too emotional for the patient to see the forest from the trees. If I am a practitioner who only sees maladies as intellectual problems to solve, the patient will sense that I actually don't know "the answer" either! The patient can sense whether or not I am able to hold space for their whole self (and not just the problem they present). When I focus on listening to the patient's entire story, we can start the healing journey together.

Trying to heal by only solving intellectual problems is a bit like trying to fight climate change by shooting things into the clouds to control weather patterns. Let us first ask, what does the Earth need from us? What is the Earth trying to tell us? Our bodies are the same. We are wild and complex beings, constantly unfolding our DNA, constantly changing and rebalancing to find homeostasis, in order to stay alive. Truth be told, my job first and foremost is to see the whole picture, the whole person before making a diagnosis. I have watched seemingly unsolvable problems spontaneously resolve, when a person feels completely safe, and seen, and heard for who they really are; mind body and spirit.